﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSync.Model
{
    public class ItemInfo
    {
        public string FileName { get; set; }

        public IEnumerable<string> Keywords { get; set; }

        public short Rating { get; set; }
    }
}
