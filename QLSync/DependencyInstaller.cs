﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLSync.Repository;

namespace QLSync
{
    public static class DependencyInstaller
    {
        private static string SOURCE_REPOSITORY = "SourceRepository";
        private static string TARGET_REPOSITORY = "TargetRepository";

        private static IUnityContainer container = new UnityContainer();

        public static void Install(bool isQnapAsSource)
        {
            container.RegisterType<IRepository, Qnap>(isQnapAsSource ? SOURCE_REPOSITORY : TARGET_REPOSITORY);
            container.RegisterType<IRepository, Lightroom>(isQnapAsSource ? TARGET_REPOSITORY : SOURCE_REPOSITORY);
            container.RegisterType<Processor>(new InjectionConstructor(
                new ResolvedParameter(typeof(IRepository), SOURCE_REPOSITORY),
                new ResolvedParameter(typeof(IRepository), TARGET_REPOSITORY)));
        }
    }
}
