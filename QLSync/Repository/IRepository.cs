﻿using QLSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSync.Repository
{
    public interface IRepository
    {
        ItemsCollection GetDataPage(int page, int pageSize);

        bool SaveData(IEnumerable<ItemInfo> items);
    }
}
