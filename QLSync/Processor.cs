﻿using Microsoft.Practices.Unity;
using QLSync.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSync
{
    public class Processor
    {
        private static int PAGE_SIZE = 20;

        public IRepository SourceRepository{ get; private set; }

        public IRepository TargetRepository { get; private set; }

        public Processor(IRepository sourceRepository, IRepository targetRepository)
        {
            this.SourceRepository = sourceRepository;
            this.TargetRepository = targetRepository;
        }

        public bool Process()
        {
            int curPage = 1;
            int pageCount = 0;
            do
            {
                var data = this.SourceRepository.GetDataPage(curPage, PAGE_SIZE);
                if (data.Length == 0)
                {
                    break;
                }
                if (pageCount == 0)
                {
                    pageCount = GetPageCount(data.Length, PAGE_SIZE);
                }

                this.TargetRepository.SaveData(data.Items);

            } while (curPage < pageCount);

            return true;
        }

        private int GetPageCount(long itemsLength, int pageSize)
        {
            int pages = (int)(itemsLength / pageSize);
            return itemsLength % pageSize == 0 ? pages : pages + 1;
        }
    }
}
